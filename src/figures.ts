export const fieldSize = { width: 10, height: 20 };
export const figures = [
  {
    label: 'line',
    matrix: [
      [0, 0, 1, 0, 0],
      [0, 0, 1, 0, 0],
      [0, 0, 1, 0, 0],
      [0, 0, 1, 0, 0],
      [0, 0, 0, 0, 0],
    ],
    color: 'black',
  },
  {
    label: 'squad',
    matrix: [
      [1, 1],
      [1, 1],
    ],
    color: 'green',
  },
  {
    label: 'trio',
    matrix: [
      [0, 1, 0],
      [0, 1, 0],
      [0, 1, 0],
    ],
    color: 'black',
  },
  {
    label: 'g',
    matrix: [
      [0, 1, 1],
      [0, 1, 0],

      [0, 1, 0],
    ],
    color: 'black',
  },
];
