import React, { useRef, useState, useCallback, useEffect } from 'react';
import cloneDeep from 'lodash.clonedeep';
import cn from 'classnames';
import classes from './game.module.scss';
import { figures, fieldSize } from '../../figures';

type FigureState = {
  label: string;
  matrix: number[][];
  position: { x: number; y: number };
};

enum GameStatuses {
  new,
  running,
  over,
}

const Game = () => {
  const timeoutId: React.MutableRefObject<NodeJS.Timeout | null> = useRef(null);
  const [paused, setPaused] = useState(false);
  const [gameCounter, setGameCounter] = useState(0);
  const [scores, setScores] = useState(0);
  const [level, setlevel] = useState(1);
  const gameField = useRef(
    Array.from({ length: fieldSize.height }, () => Array.from({ length: fieldSize.width })),
  );
  const [figure, setFigure] = useState<FigureState | null>(null);
  const [gameStatus, setGameStatus] = useState(GameStatuses.new);
  const getRandomInt = (max: number): number => Math.floor(Math.random() * max);

  const generateFigure = useCallback(() => {
    const randomFigure = figures[getRandomInt(figures.length)];
    const position = {
      x: Math.floor(fieldSize.width / 2 - randomFigure.matrix[0].length / 2),
      y: 0,
    };
    return { ...randomFigure, position };
  }, []);

  const figureAvailable = useCallback(
    (figureToCheck: FigureState) =>
      !figureToCheck.matrix.some((row, rowIndex) =>
        row.some((ceil, ceilIndex) => {
          const rowPosOnField = rowIndex + figureToCheck.position.y;
          const ceilPosOnField = ceilIndex + figureToCheck.position.x;

          return (
            ceil === 1 &&
            // row offset not available
            (rowPosOnField >= gameField.current.length ||
              // ceil offset
              ceilPosOnField >= gameField.current[rowPosOnField].length ||
              ceilPosOnField < 0 ||
              // gameField already contains point
              gameField.current[rowPosOnField][ceilPosOnField] === 1)
          );
        }),
      ),
    [gameField],
  );

  const rotateFigure = (figureToRotate: FigureState) => {
    const rowsLength = figureToRotate.matrix[0].length;
    const columnsLength = figureToRotate.matrix.length;
    const rotatedFigure: FigureState = {
      label: figureToRotate.label,
      matrix: Array.from({ length: rowsLength }, () => Array.from({ length: columnsLength })),
      position: {
        x: Math.floor(
          figureToRotate.position.x + (columnsLength - figureToRotate.matrix[0].length) / 2,
        ),
        y: Math.floor(figureToRotate.position.y + (rowsLength - figureToRotate.matrix.length) / 2),
      },
    } as FigureState;
    rotatedFigure.matrix.forEach((row, rowIndex) => {
      row.forEach((ceil, ceilIndex) => {
        rotatedFigure.matrix[rowIndex][ceilIndex] =
          figureToRotate.matrix[figureToRotate.matrix.length - 1 - ceilIndex][rowIndex];
      });
    });
    return rotatedFigure;
  };
  const restartGame = () => {
    gameField.current = Array.from({ length: fieldSize.height }, () =>
      Array.from({ length: fieldSize.width }),
    );
    setFigure(null);
    setGameStatus(GameStatuses.running);
    if (timeoutId.current) {
      clearTimeout(timeoutId.current);
    }
    setGameCounter(1);
  };

  // game process
  useEffect(() => {
    const keyReaction = (e: KeyboardEvent) => {
      if (figure && gameStatus === GameStatuses.running) {
        let newPosition;
        let newFigure = figure;
        switch (e.code) {
          case 'ArrowUp':
            newFigure = rotateFigure(newFigure);
            newPosition = newFigure.position;
            break;
          case 'ArrowDown':
            newPosition = {
              x: figure.position.x,
              y: figure.position.y + 1,
            };
            break;
          case 'ArrowLeft':
            newPosition = {
              x: figure.position.x - 1,
              y: figure.position.y,
            };
            break;
          case 'ArrowRight':
            newPosition = {
              x: figure.position.x + 1,
              y: figure.position.y,
            };
            break;
          default:
            newPosition = {
              x: figure.position.x,
              y: figure.position.y,
            };
        }
        if (figureAvailable({ ...newFigure, position: newPosition })) {
          setFigure({ ...newFigure, position: newPosition });
        }
      }
    };
    window.addEventListener('keydown', keyReaction);
    return () => {
      window.removeEventListener('keydown', keyReaction);
    };
  }, [figure, figureAvailable, gameStatus]);

  // increase game level
  useEffect(() => {
    setlevel(Math.floor(scores / 1000));
  }, [scores]);

  const sanitizeField = (field: number[][]) => {
    const newField = cloneDeep(field).filter((row) => !row.every((ceil) => ceil === 1));
    setScores((prevScores) => prevScores + (fieldSize.height - newField.length) * 100);
    return Array.from({ length: fieldSize.height - newField.length }, () =>
      Array.from({ length: fieldSize.width }),
    ).concat(newField);
  };

  const addFigureToField = useCallback(
    (figureToAdd: FigureState) => {
      const newField = cloneDeep(gameField.current);
      figureToAdd.matrix.forEach((row, rowIndex) => {
        row.forEach((ceil, ceilIndex) => {
          if (ceil === 1) {
            newField[rowIndex + figureToAdd.position.y][ceilIndex + figureToAdd.position.x] = 1;
          }
        });
      });
      gameField.current = sanitizeField(newField as number[][]);
    },
    [gameField],
  );

  const gameAction = useCallback(() => {
    const setNewFigure = () => {
      const newFigure = generateFigure();
      if (figureAvailable(newFigure)) {
        setFigure(newFigure);
      } else {
        setGameStatus(GameStatuses.over);
      }
    };
    if (figure === null) {
      setNewFigure();
    } else {
      const newFigure = cloneDeep(figure);
      newFigure.position.y += 1;
      if (figureAvailable(newFigure)) {
        setFigure(newFigure);
      } else {
        addFigureToField(figure);
        setNewFigure();
      }
    }
    if (gameStatus === GameStatuses.running) {
      timeoutId.current = setTimeout(
        () => setGameCounter((prevGameCounter) => prevGameCounter + 1),
        Math.max(1000 - level * 100, 100),
      );
    }
  }, [gameStatus, figure, addFigureToField, figureAvailable, level, generateFigure]);

  useEffect(() => {
    if (gameCounter > 0 && !paused) {
      gameAction();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [gameCounter, paused]);

  const elId = (key: number) => key;
  const isUnderFigure = (rowIndex: number, cellIndex: number, overFigure: FigureState) =>
    rowIndex - overFigure.position.y >= 0 &&
    rowIndex - overFigure.position.y < overFigure.matrix.length &&
    cellIndex - overFigure.position.x >= 0 &&
    cellIndex - overFigure.position.x <
      overFigure.matrix[rowIndex - overFigure.position.y].length &&
    overFigure.matrix[rowIndex - overFigure.position.y][cellIndex - overFigure.position.x] === 1;

  return (
    <div className={classes.body}>
      <div
        className={cn({
          [classes.game]: true,
          [classes.finished]: gameStatus === GameStatuses.over,
        })}
      >
        {gameField.current.map((row, rowIndex) => (
          <div className={classes.row} key={elId(rowIndex)}>
            {row.map((cell, cellIndex) => (
              <div
                className={cn({
                  [classes.ceil]: true,
                  [classes.filled]: cell === 1,
                  [classes.underFigure]:
                    figure !== null && isUnderFigure(rowIndex, cellIndex, figure),
                })}
                key={elId(cellIndex)}
              />
            ))}
          </div>
        ))}
        {gameStatus === GameStatuses.over ? (
          <div className={classes.finished}>
            <div>Game Over</div>
            <div>{scores}</div>
          </div>
        ) : null}
      </div>
      <div className={classes.controls}>
        <button
          className={classes.startButton}
          onClick={() => {
            setGameStatus(GameStatuses.running);
            setGameCounter(1);
          }}
        >
          Start
        </button>
        <button
          className={classes.pauseButton}
          onClick={() => {
            setPaused((prev) => !prev);
          }}
        >
          {paused ? 'Resume' : 'Pause'}
        </button>
        <button className={classes.restartButton} onClick={() => restartGame()}>
          Restart
        </button>
        <div>Scores {scores}</div>
        <div>Level {level}</div>
      </div>
    </div>
  );
};

export default Game;
